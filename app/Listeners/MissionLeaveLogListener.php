<?php

namespace App\Listeners;

use App\Events\MissionLeftEvent;
use App\Events\MissionMessageEvent;
use App\Models\MissionMessage;

class MissionLeaveLogListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(MissionLeftEvent $event): void
    {
        $message = MissionMessage::query()->create([
            'type' => 'log',
            'message' => 'User: ' . $event->user->name . ' left the chat.',
            'sender_id' => $event->user->id,
            'mission_id' => $event->mission->id,
        ]);
        $message = MissionMessage::query()->with('sender:id,name')->find($message->id);

        MissionMessageEvent::dispatch($event->mission, $message);
    }
}
