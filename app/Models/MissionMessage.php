<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MissionMessage extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function mission()
    {
        return $this->belongsTo(Mission::class);
    }

    public function sender()
    {
        return $this->belongsTo(User::class);
    }
}
