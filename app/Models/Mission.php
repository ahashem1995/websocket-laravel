<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Mission extends Model
{
    use HasFactory;

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'mission_user');
    }

    public function messages(): HasMany
    {
        return $this->hasMany(MissionMessage::class);
    }

    public function hasUser(User $user): bool {
        return $this->users()->find($user->id) !== null;
    }
}
