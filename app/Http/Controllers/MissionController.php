<?php

namespace App\Http\Controllers;

use App\Events\MissionJoinedEvent;
use App\Events\MissionLeftEvent;
use App\Models\Mission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        $missions = Mission::with(['users:id'])->get()
            ->map(function ($mission) use($user) {
                $mission->joined = in_array($user->id, $mission->users->pluck('id')->toArray());
                unset($mission->users);
                return $mission;
            });
        return response()->json($missions);
    }

    public function joinMission(Request $request, Mission $mission)
    {
        $user = Auth::user();
        $mission->users()->attach($user);

        MissionJoinedEvent::dispatch($mission, $user);

        return response()->json(['message' => 'User joined mission']);
    }

    public function leaveMission(Request $request, Mission $mission)
    {
        $user = Auth::user();
        $mission->users()->detach($user);

        MissionLeftEvent::dispatch($mission, $user);

        return response()->json(['message' => 'User left mission']);
    }
}
