<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('token')->plainTextToken;
            $success['user']['id'] = $user->id;
            $success['user']['name'] = $user->name;
            $success['user']['email'] = $user->email;

            return response()->json($success);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Email/Password combination is incorrect.',
            ], 404);
        }
    }

    public function logout()
    {
        Auth::user()->currentAccessToken()->delete();

        return response()->json([
            'success' => true,
            'message' => 'Logged out successfully.'
        ]);
    }
}
