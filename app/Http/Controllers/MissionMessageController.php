<?php

namespace App\Http\Controllers;

use App\Events\MissionMessageEvent;
use App\Models\Mission;
use App\Models\MissionMessage;
use Illuminate\Support\Facades\Auth;

class MissionMessageController extends Controller
{
    public function getMissionMessages(Mission $mission)
    {
        $authorized = $mission->hasUser(Auth::user());
        if (!$authorized) {
            return response()->json([
                'success' => false,
                'message' => 'You are not authorized to view this chat'
            ], 401);
        }
        return response()->json($mission->messages()->with('sender:id,name')->get());
    }

    public function postMissionMessage(Mission $mission)
    {
        $message = \request('message');
        $missionMessage = MissionMessage::query()->create([
            'type' => 'message',
            'message' => $message,
            'sender_id' => Auth::id(),
            'mission_id' => $mission->id,
        ]);
        $missionMessage = MissionMessage::query()->with('sender:id,name')
            ->find($missionMessage->id);

        MissionMessageEvent::dispatch($mission, $missionMessage);

        return response()->json(['success' => true], 201);
    }
}
