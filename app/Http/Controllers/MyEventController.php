<?php

namespace App\Http\Controllers;

use App\Events\MyEvent;
use App\Models\MyData;

class MyEventController extends Controller
{
    function getEvent() {
        MyEvent::dispatch(new MyData());
        return response()->json([
            'data' => true,
            'response' => 'success',
        ]);
    }
}
