<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MissionController;
use App\Http\Controllers\MissionMessageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [AuthController::class, 'login'])->name('login');


Route::middleware('auth:sanctum')->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('/missions', [MissionController::class, 'index']);
    Route::get('/missions/{mission}/messages', [MissionMessageController::class, 'getMissionMessages']);
    Route::post('/missions/{mission}/messages', [MissionMessageController::class, 'postMissionMessage']);
    Route::post('/missions/{mission}/join', [MissionController::class, 'joinMission']);
    Route::post('/missions/{mission}/leave', [MissionController::class, 'leaveMission']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
